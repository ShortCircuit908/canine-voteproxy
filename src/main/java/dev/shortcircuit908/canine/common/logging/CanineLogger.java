package dev.shortcircuit908.canine.common.logging;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Date;
import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class CanineLogger {
	public static Logger getLogger(String name) {
		Logger logger = Logger.getLogger(name);
		logger.setUseParentHandlers(false);
		ConsoleHandler handler = new ConsoleHandler();
		handler.setFormatter(new CanineFormatter());
		logger.addHandler(handler);
		return logger;
	}
	
	public static void rerouteSystemOut() {
		Logger system_logger = getLogger("System.out");
		System.setOut(new LoggingPrintWriter(system_logger, Level.INFO));
	}
	
	public static void rerouteSystemErr() {
		Logger system_logger = getLogger("System.err");
		System.setErr(new LoggingPrintWriter(system_logger, Level.WARNING));
	}
	
	private static class LoggingOutputStream extends OutputStream {
		private final Logger logger;
		private final Level log_level;
		private final StringBuffer buffer = new StringBuffer(1024);
		
		public LoggingOutputStream(Logger logger, Level log_level) {
			this.logger = logger;
			this.log_level = log_level;
		}
		
		@Override
		public void write(int b) {
			if (b == '\n') {
				logger.log(log_level, buffer.toString());
				buffer.setLength(0);
			}
			else if (b != '\r') {
				buffer.append((char) b);
			}
		}
	}
	
	private static class LoggingPrintWriter extends PrintStream {
		public LoggingPrintWriter(Logger logger, Level log_level) {
			super(new LoggingOutputStream(logger, log_level));
		}
	}
	
	private static class CanineFormatter extends Formatter {
		private static final String format = "[%1$tF %1$tT] [%2$s/%3$s] %4$s %n";
		
		@Override
		public String format(LogRecord record) {
			return String.format(
					format,
					new Date(record.getMillis()),
					Thread.getAllStackTraces()
							.keySet()
							.stream()
							.filter(thread -> thread.getId() == record.getThreadID())
							.findAny()
							.get()
							.getName(),
					record.getLevel().getLocalizedName(),
					record.getMessage()
			);
		}
	}
}
