package dev.shortcircuit908.canine.voteproxy.effects;

import dev.shortcircuit908.canine.common.logging.CanineLogger;
import dev.shortcircuit908.canine.voteproxy.CanineConfig;
import dev.shortcircuit908.canine.voteproxy.GameDetector;
import dev.shortcircuit908.canine.voteproxy.WeightedRandomBag;
import dev.shortcircuit908.canine.voteproxy.twitch.CanineTwitchClient;
import dev.shortcircuit908.canine.voteproxy.twitch.listener.SubListener;
import dev.shortcircuit908.canine.voteproxy.voting.VoteSystem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

public class EffectDispatcher {
	private final Logger logger = CanineLogger.getLogger("EffectDispatcher");
	private Timer system_timer;
	private final CanineConfig config;
	private final GameDetector game_detector;
	private final CanineTwitchClient twitch_client;
	private final VoteSystem vote_system;
	private final Random random = new Random();
	private String last_effect = null;
	private boolean alternate_index = true;
	
	private final Map<String, Effect> available_effects = new HashMap<>();
	private final Map<String, Effect> vote_choices = new HashMap<>();
	
	public EffectDispatcher(CanineConfig config) {
		this.config = config;
		this.game_detector = new GameDetector(config);
		this.twitch_client = new CanineTwitchClient(config);
		this.vote_system = new VoteSystem(twitch_client);
		twitch_client.registerSubEventListener(new SubListener(this));
		game_detector.registerGameListener(game -> {
			vote_system.announceGame(game.getName());
			startVoting();
		});
	}
	
	private void initAvailableEffects() {
		available_effects.clear();
		available_effects.putAll(config.getEffects().getOrDefault("{global}", new HashMap<>()));
		int global_effects = available_effects.size();
		CanineConfig.GameInfo game = game_detector.getCurrentGame();
		if (game != null) {
			available_effects.putAll(config.getEffects().getOrDefault(game.getName(), new HashMap<>()));
		}
		int specific_effects = available_effects.size() - global_effects;
		logger.info(String.format(
				"Loaded %1$s global effects and %2$s game effects",
				global_effects,
				specific_effects
		));
	}
	
	public void start() {
		game_detector.start();
		
	}
	
	private void startVoting() {
		if (system_timer != null) {
			system_timer.cancel();
			system_timer.purge();
		}
		system_timer = new Timer("EffectDispatcher");
		system_timer.scheduleAtFixedRate(
				new TaskStartVote(),
				0,
				config.getVoteDurationMillis() + config.getVoteCooldownMillis()
		);
	}
	
	private class TaskStartVote extends TimerTask {
		@Override
		public void run() {
			logger.info("Starting vote");
			alternate_index = !alternate_index;
			initAvailableEffects();
			Map<String, Effect> local_effects = new HashMap<>(available_effects);
			if (last_effect != null) {
				local_effects.remove(last_effect);
			}
			int num_choices = Math.min(config.getChoicesPerVote(), local_effects.size());
			List<Map.Entry<String, Effect>> effect_list = new ArrayList<>(local_effects.entrySet());
			Collections.shuffle(effect_list, random);
			String[] choices = effect_list.stream().limit(num_choices).map(Map.Entry::getKey).toArray(String[]::new);
			vote_system.startVote(choices, alternate_index ? config.getChoicesPerVote() : 0);
			vote_system.announceVote();
			system_timer.schedule(new TaskGetVoteResults(), config.getVoteDurationMillis());
		}
	}
	
	private class TaskGetVoteResults extends TimerTask {
		@Override
		public void run() {
			logger.info("Collecting vote results");
			Map<Integer, Integer> results = vote_system.getIndexedVoteResults();
			WeightedRandomBag<Integer> bag = new WeightedRandomBag<>();
			results.forEach(bag::addEntry);
			int selected_index = bag.selectRandom(random);
			String effect = vote_system.getVoteChoices()[selected_index];
			logger.info(effect + " won the vote");
			last_effect = effect;
			Effect to_dispatch = available_effects.get(effect);
			logger.info("Dispatching effect: " + effect);
			dispatchEffect(to_dispatch);
		}
	}
	
	public void dispatchRandomEffect() {
		List<Effect> effects = new ArrayList<>(available_effects.values());
		if (effects.size() < 1) {
			return;
		}
		int index = random.nextInt(effects.size());
		dispatchEffect(effects.get(index));
	}
	
	public void dispatchEffect(Effect effect) {
		if (effect.getStartCommands() != null) {
			system_timer.schedule(new TaskDispatchCommand(
					game_detector.getCurrentGame(),
					effect.getStartCommands()
			), 0);
		}
		long duration_millis = effect.getDuration().orElse(0) * 1000L;
		String[] end_commands = effect.getEndCommands().orElse(null);
		if (end_commands != null && duration_millis > 0) {
			system_timer.schedule(new TaskDispatchCommand(
					game_detector.getCurrentGame(),
					end_commands
			), duration_millis);
		}
	}
	
	private static class TaskDispatchCommand extends TimerTask {
		private final CanineConfig.GameInfo game;
		private final String[] commands;
		
		private TaskDispatchCommand(CanineConfig.GameInfo game, String[] commands) {
			this.game = game;
			this.commands = commands;
		}
		
		@Override
		public void run() {
			for (String command : commands) {
				InputHelper.type(game.formatCommand(command));
			}
		}
	}
	
	public void stop() {
		twitch_client.close();
		game_detector.stop();
		if(system_timer != null) {
			system_timer.cancel();
			system_timer.purge();
		}
	}
}
