package dev.shortcircuit908.canine.voteproxy.effects;

import java.util.Optional;

public class Effect {
	private final String[] start_commands;
	private final String[] end_commands;
	private final Integer duration;
	
	Effect(){
		this.start_commands = null;
		this.end_commands = null;
		this.duration = null;
	}
	
	public Effect(String... commands){
		this(commands, null, null);
	}
	
	public Effect(String[] start_commands, String[] end_commands, Integer duration){
		this.start_commands = start_commands;
		this.end_commands = end_commands;
		this.duration = duration;
	}
	
	public String[] getStartCommands(){
		return start_commands;
	}
	
	public Optional<String[]> getEndCommands(){
		return end_commands == null || end_commands.length == 0 ? Optional.empty() : Optional.of(end_commands);
	}
	
	public Optional<Integer> getDuration(){
		return duration != null && duration > 0 ? Optional.of(duration) : Optional.empty();
	}
}
