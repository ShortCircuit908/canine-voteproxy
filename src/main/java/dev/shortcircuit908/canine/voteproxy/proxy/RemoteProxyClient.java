package dev.shortcircuit908.canine.voteproxy.proxy;

import dev.shortcircuit908.canine.common.logging.CanineLogger;
import dev.shortcircuit908.canine.voteproxy.proxy.data.Packet;
import dev.shortcircuit908.canine.voteproxy.proxy.data.PacketRequestVoteResults;
import dev.shortcircuit908.canine.voteproxy.proxy.data.PacketSetGame;
import dev.shortcircuit908.canine.voteproxy.proxy.data.PacketStartVote;
import dev.shortcircuit908.canine.voteproxy.proxy.data.PacketVoteResults;
import dev.shortcircuit908.canine.voteproxy.voting.VoteSystem;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

public class RemoteProxyClient implements AutoCloseable {
	private static final Logger logger = CanineLogger.getLogger("RemoteProxyClient");
	private final Socket socket;
	private final BufferedReader in;
	private final PrintWriter out;
	private final VoteSystem vote_system;
	
	public RemoteProxyClient(Socket socket, VoteSystem vote_system) throws IOException {
		this.socket = socket;
		this.vote_system = vote_system;
		this.in = new BufferedReader(new InputStreamReader(socket.getInputStream(), StandardCharsets.UTF_8));
		this.out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), StandardCharsets.UTF_8));
	}
	
	public void start() {
		Thread thread = new Thread(new InputHandler());
		thread.start();
		thread.setName("RemoteProxyClient-" + thread.getId());
		logger.info("Ready to receive packets");
	}
	
	@Override
	public void close() throws IOException {
		in.close();
		out.close();
		socket.close();
	}
	
	private void processPacket(Packet packet) throws IOException {
		if (packet instanceof PacketStartVote) {
			PacketStartVote conv_packet = (PacketStartVote) packet;
			vote_system.startVote(conv_packet.getVoteOptions(), conv_packet.getIndexOffset());
			vote_system.announceVote();
			return;
		}
		if (packet instanceof PacketRequestVoteResults) {
			PacketVoteResults results =
					new PacketVoteResults(vote_system.getVoteChoices(), vote_system.getIndexedVoteResults());
			String raw_packet = Packet.writePacket(results);
			out.write(raw_packet);
			out.write('\n');
		}
		if (packet instanceof PacketSetGame) {
			PacketSetGame conv_packet = (PacketSetGame) packet;
			//vote_system.announceGame();
		}
	}
	
	private class InputHandler implements Runnable {
		@Override
		public void run() {
			while (!out.checkError()) {
				try {
					String line = in.readLine();
					if (line == null) {
						break;
					}
					Packet packet = Packet.readPacket(line);
					if (packet == null) {
						logger.warning("Bad packet: " + line);
						continue;
					}
					logger.info("Received packet: " + packet.getType());
					processPacket(packet);
				}
				catch (SocketException e){
					if(e.getMessage().equalsIgnoreCase("connection reset")){
						break;
					}
				}
				catch (IOException e) {
					e.printStackTrace();
				}
			}
			logger.info("Connection closed");
			try {
				close();
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
