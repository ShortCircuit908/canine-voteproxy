package dev.shortcircuit908.canine.voteproxy.proxy.data;

public class PacketStartVote extends Packet {
	private String[] vote_options;
	private int index_offset;
	
	public PacketStartVote(){
		super(PacketType.START_VOTE);
	}
	
	public PacketStartVote(String[] vote_options, int index_offset){
		this();
		this.vote_options = vote_options;
		this.index_offset = index_offset;
	}
	
	public String[] getVoteOptions(){
		return vote_options;
	}
	
	public int getIndexOffset(){
		return index_offset;
	}
}
