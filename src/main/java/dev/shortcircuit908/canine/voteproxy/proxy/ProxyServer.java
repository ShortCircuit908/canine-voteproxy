package dev.shortcircuit908.canine.voteproxy.proxy;

import dev.shortcircuit908.canine.common.logging.CanineLogger;
import dev.shortcircuit908.canine.voteproxy.CanineConfig;
import dev.shortcircuit908.canine.voteproxy.voting.VoteSystem;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;

public class ProxyServer implements AutoCloseable {
	private static final Logger logger = CanineLogger.getLogger("ProxyServer");
	private final CanineConfig config;
	private final VoteSystem vote_system;
	private final ServerSocket server_socket;
	private final AtomicBoolean started = new AtomicBoolean(false);
	private final AtomicBoolean accepting_connections = new AtomicBoolean(false);
	
	public ProxyServer(CanineConfig config, VoteSystem vote_system) throws IOException {
		this.config = config;
		this.vote_system = vote_system;
		server_socket = null;
		//server_socket = new ServerSocket(config.getHostConfig().getPort(), 10, config.getHostConfig().getAddress());
		//server_socket.setSoTimeout(1000);
	}
	
	public void start() {
		Thread thread = new Thread(new ProxyClientSink());
		thread.setName("ProxyClient-" + thread.getId());
		thread.start();
	}
	
	private class ProxyClientSink implements Runnable {
		@Override
		public void run() {
			started.set(true);
			accepting_connections.set(true);
			logger.info("Accepting connections...");
			while (started.get()) {
				try {
					Socket socket = server_socket.accept();
					if (!accepting_connections.get()) {
						logger.info(String.format(
								"Rejecting connection from %1$s - Not accepting connections",
								socket.getRemoteSocketAddress()
						));
						socket.close();
						continue;
					}
					logger.info(String.format("New connection from %1$s", socket.getRemoteSocketAddress()));
					RemoteProxyClient client = new RemoteProxyClient(socket, vote_system);
					client.start();
				}
				catch (SocketTimeoutException e) {
					// Do nothing
				}
				catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	@Override
	public void close() throws Exception {
		logger.info("No longer accepting connections");
		started.set(false);
		accepting_connections.set(false);
		if (server_socket != null && !server_socket.isClosed()) {
			server_socket.close();
		}
	}
}
