package dev.shortcircuit908.canine.voteproxy.proxy.data;

public class PacketRequestVoteResults extends Packet {
	public PacketRequestVoteResults() {
		super(PacketType.REQUEST_VOTE_RESULTS);
	}
}
