package dev.shortcircuit908.canine.voteproxy.proxy.data;

import java.util.Map;

public class PacketVoteResults extends Packet {
	private String[] vote_options;
	private Map<Integer, Integer> indexed_vote_results;
	
	public PacketVoteResults() {
		super(PacketType.VOTE_RESULTS);
	}
	
	public PacketVoteResults(String[] vote_options, Map<Integer, Integer> indexed_vote_results) {
		this();
		this.vote_options = vote_options;
		this.indexed_vote_results = indexed_vote_results;
	}
	
	public String[] getVoteOptions() {
		return vote_options;
	}
	
	public Map<Integer, Integer> getIndexedVoteResults() {
		return indexed_vote_results;
	}
}
