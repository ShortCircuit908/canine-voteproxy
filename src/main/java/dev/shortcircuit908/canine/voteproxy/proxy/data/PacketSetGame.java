package dev.shortcircuit908.canine.voteproxy.proxy.data;

public class PacketSetGame extends Packet {
	private String game;
	
	public PacketSetGame() {
		super(PacketType.SET_GAME);
	}
	
	public PacketSetGame(String game){
		this();
		this.game = game;
	}
	
	public String getGame(){
		return game;
	}
}
