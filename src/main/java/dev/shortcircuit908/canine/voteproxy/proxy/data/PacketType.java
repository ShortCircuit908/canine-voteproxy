package dev.shortcircuit908.canine.voteproxy.proxy.data;

public enum PacketType {
	REQUEST_VOTE_RESULTS(PacketRequestVoteResults.class),
	SET_GAME(PacketSetGame.class),
	START_VOTE(PacketStartVote.class),
	VOTE_RESULTS(PacketVoteResults.class);
	
	private final Class<? extends Packet> packet_class;
	
	PacketType(Class<? extends Packet> packet_class){
		this.packet_class = packet_class;
	}
	
	public Class<? extends Packet> getPacketClass(){
		return packet_class;
	}
	
	public static PacketType fromString(String name){
		for(PacketType type : values()){
			if(type.name().equalsIgnoreCase(name)){
				return type;
			}
		}
		return null;
	}
}
