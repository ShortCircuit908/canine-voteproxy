package dev.shortcircuit908.canine.voteproxy.proxy.data;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

public abstract class Packet {
	private static final ObjectMapper object_mapper = new ObjectMapper();
	
	static {
		object_mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
		object_mapper.setVisibility(PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
		object_mapper.setVisibility(PropertyAccessor.SETTER, JsonAutoDetect.Visibility.NONE);
	}
	
	private final PacketType type;
	
	public Packet(PacketType type) {
		this.type = type;
	}
	
	public PacketType getType(){
		return type;
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends Packet> T readPacket(String raw_packet) throws IOException {
		JsonNode node = object_mapper.readTree(raw_packet);
		PacketType type = PacketType.fromString(node.findValue("type").asText());
		if(type == null){
			return null;
		}
		Class<? extends Packet> packet_class = type.getPacketClass();
		return (T) object_mapper.treeToValue(node, packet_class);
	}
	
	public static String writePacket(Packet packet) throws JsonProcessingException {
		return object_mapper.writeValueAsString(packet);
	}
}
