package dev.shortcircuit908.canine.voteproxy;

import com.sun.jna.Native;

public class WindowsHelper {
	private static final int max_title_length = 1024;
	
	public static String getForegroundWindow(){
		char[] buffer = new char[max_title_length * 2];
		com.sun.jna.platform.win32.WinDef.HWND hwnd = com.sun.jna.platform.win32.User32.INSTANCE.GetForegroundWindow();
		com.sun.jna.platform.win32.User32.INSTANCE.GetWindowText(hwnd, buffer, max_title_length);
		return Native.toString(buffer);
	}
}
