package dev.shortcircuit908.canine.voteproxy.voting;

import com.github.twitch4j.chat.events.channel.ChannelMessageEvent;
import dev.shortcircuit908.canine.common.logging.CanineLogger;
import dev.shortcircuit908.canine.voteproxy.twitch.CanineTwitchClient;
import dev.shortcircuit908.canine.voteproxy.twitch.listener.ChatListener;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;
import java.util.logging.Logger;

public class VoteSystem {
	private static final Logger logger = CanineLogger.getLogger("VoteSystem");
	private final CanineTwitchClient twitch_client;
	private String[] vote_choices;
	private Map<String, Integer> ballot_box = new HashMap<>();
	private int index_offset;
	
	public VoteSystem(CanineTwitchClient twitch_client) {
		this.twitch_client = twitch_client;
		registerEventListeners();
	}
	
	public String[] getVoteChoices() {
		return vote_choices;
	}
	
	public Map<Integer, Integer> getIndexedVoteResults() {
		Map<Integer, Integer> results = new HashMap<>();
		for(int i = 0; i < vote_choices.length; i++){
			results.put(i, 1);
		}
		for (int index : ballot_box.values()) {
			results.compute(index, (key, old_value) -> old_value == null ? 1 : old_value + 1);
		}
		return results;
	}
	
	public int getIndexOffset() {
		return index_offset;
	}
	
	public void castVote(String user_id, int indexed_vote) {
		if (indexed_vote < 0 || indexed_vote >= vote_choices.length){
			return;
		}
		logger.info(String.format("%1$s voted for index %2$s", user_id, indexed_vote));
		ballot_box.put(user_id, indexed_vote);
	}
	
	public void registerEventListeners() {
		twitch_client.getClient()
				.getEventManager()
				.onEvent(ChannelMessageEvent.class, new ChatListener(twitch_client, this));
	}
	
	public void announceGame(String game) {
		logger.info(String.format("Announcing game to Twitch chat: %1$s", game));
		twitch_client.sendChatMessage(String.format("The streamer is now playing %1$s", game));
	}
	
	public void announceVote() {
		StringJoiner joiner = new StringJoiner(" | ");
		for (int i = 0; i < vote_choices.length; i++) {
			joiner.add(String.format("%1$s:%2$s", (i + index_offset) + 1, vote_choices[i]));
		}
		logger.info(String.format("Announcing vote to Twitch chat: %1$s", joiner));
		twitch_client.sendChatMessage("Voting has begun! Type the number in chat to cast your vote!");
		twitch_client.sendChatMessage(joiner.toString());
	}
	
	public void startVote(String[] vote_choices, int index_offset) {
		logger.info("New vote: " + Arrays.toString(vote_choices));
		this.vote_choices = vote_choices;
		this.index_offset = index_offset;
		ballot_box.clear();
	}
}
