package dev.shortcircuit908.canine.voteproxy;

import dev.shortcircuit908.canine.common.logging.CanineLogger;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.function.Consumer;
import java.util.logging.Logger;

public class GameDetector {
	private final Logger logger = CanineLogger.getLogger("GameDetector");
	private boolean started = false;
	private final CanineConfig config;
	private final Timer timer = new Timer("GameDetector");
	private CanineConfig.GameInfo current_game;
	private Set<Consumer<CanineConfig.GameInfo>> game_change_listeners = new HashSet<>();
	
	public GameDetector(CanineConfig config) {
		this.config = config;
	}
	
	public void registerGameListener(Consumer<CanineConfig.GameInfo> listener) {
		game_change_listeners.add(listener);
	}
	
	public CanineConfig.GameInfo getCurrentGame() {
		return current_game;
	}
	
	public void start() {
		if (started) {
			return;
		}
		timer.schedule(new DetectorTask(), 0, config.getGameDetectorIntervalMillis());
		started = true;
		logger.info("Game detector started");
	}
	
	public void stop() {
		timer.cancel();
		timer.purge();
		logger.info("Game detector stopped");
	}
	
	private class DetectorTask extends TimerTask {
		@Override
		public void run() {
			String window = WindowsHelper.getForegroundWindow();
			for (Map.Entry<String, CanineConfig.GameInfo> game : config.getGames().entrySet()) {
				if (window.contains(game.getKey())) {
					CanineConfig.GameInfo new_game = game.getValue();
					if (current_game != null && current_game.getName().equalsIgnoreCase(new_game.getName())) {
						break;
					}
					logger.info("Detected new game: " + new_game.getName());
					current_game = new_game;
					for (Consumer<CanineConfig.GameInfo> listener : game_change_listeners) {
						listener.accept(new_game);
					}
					break;
				}
			}
		}
	}
}
