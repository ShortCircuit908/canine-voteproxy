package dev.shortcircuit908.canine.voteproxy.twitch;

import com.github.philippheuer.credentialmanager.domain.OAuth2Credential;
import com.github.twitch4j.TwitchClient;
import com.github.twitch4j.TwitchClientBuilder;
import com.github.twitch4j.chat.events.channel.ExtendSubscriptionEvent;
import com.github.twitch4j.chat.events.channel.GiftSubscriptionsEvent;
import com.github.twitch4j.chat.events.channel.PrimeSubUpgradeEvent;
import com.github.twitch4j.chat.events.channel.SubscriptionEvent;
import dev.shortcircuit908.canine.common.logging.CanineLogger;
import dev.shortcircuit908.canine.voteproxy.CanineConfig;
import dev.shortcircuit908.canine.voteproxy.twitch.listener.CanineSubEvent;
import dev.shortcircuit908.canine.voteproxy.twitch.listener.SubListener;

import java.io.Closeable;
import java.util.logging.Logger;

public class CanineTwitchClient implements Closeable {
	private final Logger logger = CanineLogger.getLogger("TwitchClient");
	private final CanineConfig config;
	private final TwitchClient client;
	
	public CanineTwitchClient(CanineConfig config) {
		this.config = config;
		OAuth2Credential credential = new OAuth2Credential(
				config.getTwitchConfig().getBotName(),
				config.getTwitchConfig().getBotOAuth()
		);
		this.client = TwitchClientBuilder.builder()
				.withChatAccount(credential)
				.withDefaultAuthToken(credential)
				.withEnablePubSub(true)
				.withEnableChat(true)
				.build();
		logger.info("Twitch client connected");
		joinChannel();
	}
	
	public TwitchClient getClient() {
		return client;
	}
	
	public void registerSubEventListener(final SubListener sub_listener) {
		client.getEventManager()
				.onEvent(GiftSubscriptionsEvent.class, event -> sub_listener.accept(new CanineSubEvent(event)));
		client.getEventManager()
				.onEvent(ExtendSubscriptionEvent.class, event -> sub_listener.accept(new CanineSubEvent(event)));
		client.getEventManager()
				.onEvent(SubscriptionEvent.class, event -> sub_listener.accept(new CanineSubEvent(event)));
		client.getEventManager()
				.onEvent(PrimeSubUpgradeEvent.class, event -> sub_listener.accept(new CanineSubEvent(event)));
	}
	
	private void joinChannel() {
		logger.info("Joining channel " + config.getTwitchConfig().getChannel());
		client.getChat().joinChannel(config.getTwitchConfig().getChannel());
	}
	
	public void sendChatMessage(String message) {
		logger.info("Sending message: " + message);
		client.getChat().sendMessage(config.getTwitchConfig().getChannel(), message);
	}
	
	@Override
	public void close() {
		logger.info("Twitch client closing");
		client.close();
	}
}
