package dev.shortcircuit908.canine.voteproxy.twitch.listener;

import com.github.twitch4j.chat.events.channel.CheerEvent;
import dev.shortcircuit908.canine.voteproxy.twitch.CanineTwitchClient;

import java.util.function.Consumer;

public class CheerListener implements Consumer<CheerEvent> {
	private final CanineTwitchClient client;
	
	public CheerListener(CanineTwitchClient client) {
		this.client = client;
	}
	
	@Override
	public void accept(CheerEvent event) {
	
	}
}
