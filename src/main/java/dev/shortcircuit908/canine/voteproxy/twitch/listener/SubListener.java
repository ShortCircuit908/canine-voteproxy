package dev.shortcircuit908.canine.voteproxy.twitch.listener;

import dev.shortcircuit908.canine.voteproxy.effects.EffectDispatcher;

import java.util.function.Consumer;

public class SubListener implements Consumer<CanineSubEvent> {
	private final EffectDispatcher effect_dispatcher;
	
	public SubListener(EffectDispatcher effect_dispatcher) {
		this.effect_dispatcher = effect_dispatcher;
	}
	
	@Override
	public void accept(CanineSubEvent event) {
		for (int i = 0; i < event.getNumSubscriptions(); i++) {
			effect_dispatcher.dispatchRandomEffect();
		}
	}
}
