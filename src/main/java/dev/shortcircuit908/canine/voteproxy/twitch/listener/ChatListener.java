package dev.shortcircuit908.canine.voteproxy.twitch.listener;

import com.github.twitch4j.chat.events.channel.ChannelMessageEvent;
import dev.shortcircuit908.canine.voteproxy.twitch.CanineTwitchClient;
import dev.shortcircuit908.canine.voteproxy.voting.VoteSystem;

import java.util.function.Consumer;

public class ChatListener implements Consumer<ChannelMessageEvent> {
	private final CanineTwitchClient client;
	private final VoteSystem vote_system;
	
	public ChatListener(CanineTwitchClient client, VoteSystem vote_system) {
		this.client = client;
		this.vote_system = vote_system;
	}
	
	@Override
	public void accept(ChannelMessageEvent event) {
		System.out.println(event.getUser().getName() + ": " + event.getMessage());
		String message= event.getMessage();
		try{
			int index = Integer.parseInt(message);
			vote_system.castVote(event.getUser().getId(), (index - vote_system.getIndexOffset()) - 1);
		}
		catch (NumberFormatException e){
			// Do nothing
		}
	}
}
