package dev.shortcircuit908.canine.voteproxy.twitch.listener;

import com.github.twitch4j.chat.events.AbstractChannelEvent;
import com.github.twitch4j.chat.events.channel.ExtendSubscriptionEvent;
import com.github.twitch4j.chat.events.channel.GiftSubscriptionsEvent;
import com.github.twitch4j.chat.events.channel.PrimeSubUpgradeEvent;
import com.github.twitch4j.chat.events.channel.SubscriptionEvent;
import com.github.twitch4j.common.enums.SubscriptionPlan;

public class CanineSubEvent {
	private final AbstractChannelEvent event;
	private final SubscriptionPlan subscription_plan;
	private final int num_subscriptions;
	
	public CanineSubEvent(GiftSubscriptionsEvent event) {
		this(event, SubscriptionPlan.fromString(event.getSubscriptionPlan()), event.getCount());
	}
	
	public CanineSubEvent(ExtendSubscriptionEvent event) {
		this(event, event.getSubPlan());
	}
	
	public CanineSubEvent(SubscriptionEvent event) {
		this(event, event.getSubPlan());
	}
	
	public CanineSubEvent(PrimeSubUpgradeEvent event) {
		this(event, event.getSubscriptionPlan());
	}
	
	private CanineSubEvent(AbstractChannelEvent event, SubscriptionPlan subscription_plan) {
		this(event, subscription_plan, 1);
	}
	
	private CanineSubEvent(AbstractChannelEvent event, SubscriptionPlan subscription_plan, int num_subscriptions) {
		this.event = event;
		this.subscription_plan = subscription_plan;
		this.num_subscriptions = num_subscriptions;
	}
	
	public AbstractChannelEvent getEvent(){
		return event;
	}
	
	public SubscriptionPlan getSubscriptionPlan(){
		return subscription_plan;
	}
	
	public int getNumSubscriptions(){
		return num_subscriptions;
	}
}
