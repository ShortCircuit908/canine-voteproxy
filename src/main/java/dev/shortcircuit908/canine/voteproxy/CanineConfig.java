package dev.shortcircuit908.canine.voteproxy;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactoryBuilder;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;
import dev.shortcircuit908.canine.voteproxy.effects.Effect;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.Map;

public class CanineConfig {
	private static final ObjectMapper object_mapper =
			new ObjectMapper(new YAMLFactoryBuilder(new YAMLFactory()).disable(YAMLGenerator.Feature.ALWAYS_QUOTE_NUMBERS_AS_STRINGS)
					.disable(YAMLGenerator.Feature.WRITE_DOC_START_MARKER)
					.disable(YAMLGenerator.Feature.USE_PLATFORM_LINE_BREAKS)
					.disable(YAMLGenerator.Feature.MINIMIZE_QUOTES)
					.build());
	
	static {
		object_mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
		object_mapper.setVisibility(PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
		object_mapper.setVisibility(PropertyAccessor.SETTER, JsonAutoDetect.Visibility.NONE);
		object_mapper.setVisibility(PropertyAccessor.IS_GETTER, JsonAutoDetect.Visibility.NONE);
	}
	
	public static CanineConfig loadConfig(Reader reader) throws IOException {
		return object_mapper.readValue(reader, CanineConfig.class);
	}
	
	public static void saveConfig(Writer writer, CanineConfig config) throws IOException {
		object_mapper.writeValue(writer, config);
	}
	
	private final Twitch twitch = new Twitch();
	private final Map<String, Map<String, Effect>> effects = MapUtil.ofEntries(
			MapUtil.entry(
					"Minecraft",
					MapUtil.ofEntries(
							MapUtil.entry(
									"Temporary Creative",
									new Effect(
											new String[]{"gamemode creative"},
											new String[]{"gamemode survival"},
											10
									)
							),
							MapUtil.entry(
									"Spawn Baddies",
									new Effect(
											"summon minecraft:creeper ~ ~10 ~",
											"summon minecraft:zombie ~ ~10 ~"
									)
							),
							MapUtil.entry(
									"Teleport to Heaven",
									new Effect(
											"teleport @p ~ ~500 ~"
									)
							),
							MapUtil.entry(
									"Temporary Spectator",
									new Effect(
											new String[]{"gamemode spectator"},
											new String[]{"gamemode survival"},
											30
									)
							),
							MapUtil.entry(
									"Greetings",
									new Effect(
											"say Hi, Chat!"
									)
							)
					)
			)
	);
	private final Map<String, GameInfo> games = MapUtil.ofEntries(
			MapUtil.entry("Skyrim Special Edition", new GameInfo("Skyrim", "`%command%%enter%`")),
			MapUtil.entry("Skyrim", new GameInfo("Skyrim", "`%command%%enter%`")),
			MapUtil.entry("Oblivion", new GameInfo("Oblivion", "`%command%%enter%`")),
			MapUtil.entry("Fallout4", new GameInfo("Fallout 4", "`%command%%enter%`")),
			MapUtil.entry("Fallout: New Vegas", new GameInfo("Fallout NV", "`%command%%enter%`")),
			MapUtil.entry("Fallout3", new GameInfo("Fallout 3", "`%command%%enter%`")),
			MapUtil.entry("Minecraft", new GameInfo("Minecraft", "/%command%%enter%")),
			MapUtil.entry("Subnautica", new GameInfo("Subnautica", "`%command%%enter%")),
			MapUtil.entry("The Witcher 3", new GameInfo("Witcher 3", "`%command%%enter%`")),
			MapUtil.entry("Valheim", new GameInfo("Valheim", "`%command%%enter%`"))
	);
	
	private final int game_detector_interval = 5;
	private final int choices_per_vote = 4;
	private final int vote_duration = 30;
	private final int vote_cooldown = 5;
	private final boolean weighted_voting = true;
	
	CanineConfig() {
	
	}
	
	public Twitch getTwitchConfig() {
		return twitch;
	}
	
	public static class GameInfo {
		private String name;
		private String command_format;
		
		GameInfo() {
		}
		
		GameInfo(String name, String command_format) {
			this.name = name;
			this.command_format = command_format;
		}
		
		public String getName() {
			return name;
		}
		
		public String getCommandFormat() {
			return command_format;
		}
		
		public String formatCommand(String command) {
			return command_format.replace("%command%", command).replace("%enter%", "\n");
		}
	}
	
	public static class Twitch {
		private String bot_oauth;
		private String bot_name;
		private String channel;
		
		public String getBotOAuth() {
			return bot_oauth;
		}
		
		public String getBotName() {
			return bot_name;
		}
		
		public String getChannel() {
			return channel;
		}
	}
	
	public long getGameDetectorIntervalMillis() {
		return game_detector_interval * 1000L;
	}
	
	public Map<String, GameInfo> getGames() {
		return games;
	}
	
	public Map<String, Map<String, Effect>> getEffects() {
		return effects;
	}
	
	public int getChoicesPerVote() {
		return choices_per_vote;
	}
	
	public long getVoteDurationMillis(){
		return vote_duration * 1000L;
	}
	
	public long getVoteCooldownMillis(){
		return vote_cooldown * 1000L;
	}
	
	public boolean isVotingWeighted(){
		return weighted_voting;
	}
}
