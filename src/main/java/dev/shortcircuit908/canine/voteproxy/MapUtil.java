package dev.shortcircuit908.canine.voteproxy;

import java.util.HashMap;
import java.util.Map;

public class MapUtil {
	@SafeVarargs
	public static <K, V> Map<K, V> ofEntries(Entry<K, V>... entries) {
		Map<K, V> map = new HashMap<>(entries.length);
		for (Entry<K, V> entry : entries) {
			map.put(entry.key, entry.value);
		}
		return map;
	}
	
	public static <K, V> Entry<K, V> entry(K key, V value) {
		return new Entry<>(key, value);
	}
	
	public static class Entry<K, V> implements Map.Entry<K, V> {
		private final K key;
		private V value;
		
		public Entry(K key) {
			this(key, null);
		}
		
		public Entry(K key, V value) {
			this.key = key;
			this.value = value;
		}
		
		@Override
		public K getKey() {
			return key;
		}
		
		@Override
		public V getValue() {
			return value;
		}
		
		@Override
		public V setValue(V value) {
			V old = this.value;
			this.value = value;
			return old;
		}
	}
}
