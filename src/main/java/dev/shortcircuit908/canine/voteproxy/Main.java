package dev.shortcircuit908.canine.voteproxy;

import dev.shortcircuit908.canine.common.logging.CanineLogger;
import dev.shortcircuit908.canine.voteproxy.effects.EffectDispatcher;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Logger;

public class Main {
	public static final Logger LOG = CanineLogger.getLogger("CanineVoteProxy");
	private static final String config_file_name = "config.yml";
	
	public static void main(String... args) throws IOException, InterruptedException {
		CanineLogger.rerouteSystemOut();
		//CanineLogger.rerouteSystemErr();
		
		File config_file = new File(config_file_name);
		File config_parent_file = config_file.getParentFile();
		if (config_parent_file != null) {
			config_parent_file.mkdirs();
		}
		CanineConfig config = new CanineConfig();
		if (!config_file.exists()) {
			try (FileWriter writer = new FileWriter(config_file)) {
				CanineConfig.saveConfig(writer, config);
			}
			LOG.info(String.format("Empty configuration file %1$s has been created", config_file_name));
			LOG.info("Please edit the configuration and relaunch the proxy");
			return;
		}
		try (FileReader reader = new FileReader(config_file)) {
			config = CanineConfig.loadConfig(reader);
		}
		catch (IOException e) {
			e.printStackTrace();
			LOG.severe(String.format("Failed to load configuration file %1$s", config_file_name));
			LOG.severe("Please fix any errors in the file, or delete it to create a new empty configuration");
			return;
		}
		
		boolean configured = true;
		if (config.getTwitchConfig().getBotOAuth() == null || config.getTwitchConfig().getBotOAuth().trim().isEmpty()) {
			LOG.severe("twitch.bot_oauth is not set");
			configured = false;
		}
		if (config.getTwitchConfig().getBotName() == null || config.getTwitchConfig().getBotName().trim().isEmpty()) {
			LOG.severe("twitch.bot_name is not set");
			configured = false;
		}
		if (config.getTwitchConfig().getChannel() == null || config.getTwitchConfig().getChannel().trim().isEmpty()) {
			LOG.severe("twitch.bot_channel is not set");
			configured = false;
		}
		if(!configured){
			LOG.severe("Please set any missing configurations and relaunch");
			return;
		}
		EffectDispatcher dispatcher = new EffectDispatcher(config);
		dispatcher.start();
		
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(System.in));
		while (true){
			String line = reader.readLine();
			if(line.equalsIgnoreCase("exit")){
				dispatcher.stop();
				return;
			}
			System.out.println("Type \"exit\" to quit");
		}
	}
}
